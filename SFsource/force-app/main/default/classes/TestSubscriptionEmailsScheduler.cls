@isTest
public class TestSubscriptionEmailsScheduler {
    
     @isTest private static void testSendEmail(){
        
         SubscriptionEmailsScheduler.inoneMonth = date.today();
         
         insert new ONB2__TriggerSettings__c(Name = 'SubscriptionBeforeInsert');
        
         // List of Account
         List<Account> accountLst = new List<Account>();
         Account acc1 = new Account();
         acc1.Name = 'TestAccount1';
         accountLst.add(acc1);
         
         Account acc2 = new Account();
         acc2.Name = 'TestAccount2';
         accountLst.add(acc2);
         
         insert accountLst;
            
         // List of subscription
         List<ONB2__Subscription__c> subLst = new List<ONB2__Subscription__c>();
         
         ONB2__Subscription__c sub1 = new ONB2__Subscription__c();
         sub1.Name = 'testSub1';
         sub1.ONB2__Status__c = 'Active';
         sub1.ONB2__EndDate__c = date.today();
         sub1.ONB2__Account__c = acc1.Id;
         subLst.add(sub1);
         
         ONB2__Subscription__c sub2 = new ONB2__Subscription__c();
         sub2.Name = 'testSub2';
         sub2.ONB2__Status__c = 'Active';
         sub2.ONB2__EndDate__c = date.today();
         sub2.ONB2__Account__c = acc2.Id;
         subLst.add(sub2);
         
         insert subLst;
         
         // insert list of Contacts
         List<Contact> contLst = new List<Contact>();
          
         Contact cont1 = new Contact();
         cont1.AccountId = acc1.Id;
         cont1.LastName = 'TestCon1';
         cont1.FirstName = 'TestContact1'; 
         cont1.Email = 'test1@gmail.com';
         contLst.add(cont1);
          
         Contact cont2 = new Contact();
         cont2.AccountId = acc2.Id;
         cont2.LastName = 'TestCon2';
         cont2.FirstName = 'TestContact2'; 
         cont2.Email = 'test2@gmail.com';
         contLst.add(cont2);
          
         insert contLst;
           
         List<Contact> contactLst = [SELECT Email FROM Contact WHERE AccountId =: accountlst LIMIT 2];
         
         List<ONB2__Subscription__c> subListId = [SELECT id 
                                                  FROM ONB2__Subscription__c 
                                                  WHERE (ONB2__EndDate__c != null) AND (ONB2__Status__c ='Active') LIMIT 2];
       
    
         Test.startTest();
         
            SubscriptionEmailsScheduler controller = new SubscriptionEmailsScheduler(); 
            
	    	controller.execute(null);
         
           Test.stopTest();

         System.assertEquals(controller.mappedToAddresses, new Map<Id, List<String>>{acc1.Id => new List<String>{'test1@gmail.com'}, 
             																		 acc2.Id => new List<String>{'test2@gmail.com'}});

    }
}