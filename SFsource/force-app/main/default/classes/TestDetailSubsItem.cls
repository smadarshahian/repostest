@isTest
public class TestDetailSubsItem { 
    
    //test Controller Constructor
    @isTest private static void testController(){
        
         insert new ONB2__TriggerSettings__c( Name = 'SubscriptionBeforeInsert' );
         ONB2__Subscription__c sub = new ONB2__Subscription__c();
         sub.Name = 'testSub';
         sub.ONB2__Status__c = 'Active';
         insert sub;
        
         PageReference testPage = Page.ViewSubPdf;
         testPage.getParameters().put('id', sub.id);
         Test.setCurrentPage(testPage); 
       
         DetailSubsItem controller = new DetailSubsItem( new ApexPages.Standardcontroller(sub) ); 
         System.assertEquals(controller.subscriptionId, sub.Id);
        
    }
    
        //test items
        @isTest private static void testItems(){
        
           insert new ONB2__TriggerSettings__c( Name = 'SubscriptionBeforeInsert' );
           ONB2__Subscription__c sub = new ONB2__Subscription__c();
           sub.Name = 'testSub';
           sub.ONB2__Status__c = 'Active';
           insert sub;
         
          // insert list of items
           List<ONB2__Item__c> itemLst = new List<ONB2__Item__c>();
          
           ONB2__Item__c item1 = new ONB2__Item__c();
           item1.ONB2__Subscription__c = sub.Id;
           item1.ONB2__BillingType__c = 'Recurring';
           item1.ONB2__Title__c = 'TestItem1';
           item1.ONB2__OrderBy__c = 'LizItem1';
           itemLst.add(item1);
          
           ONB2__Item__c item2 = new ONB2__Item__c();
           item2.ONB2__Subscription__c = sub.Id;
           item2.ONB2__BillingType__c = 'Recurring';
           item2.ONB2__Title__c = 'TestItem2'; 
           item2.ONB2__OrderBy__c = 'LizItem2';
           itemLst.add(item2);
           insert itemLst;
            
           List<ONB2__Item__c> itemList = [SELECT Id, ONB2__Title__c, ONB2__Description__c, ONB2__Quantity__c, ONB2__BillingType__c 
                                           FROM ONB2__Item__c 
                                           WHERE ONB2__Subscription__c =: Sub.Id LIMIT 2]; 
       
           DetailSubsItem controller = new DetailSubsItem(new ApexPages.Standardcontroller(sub)); 
           System.assertEquals(controller.getItems(), itemList);
           
    } 
    
         //test sendEmail
    @isTest private static void testSendEmail(){
        
         insert new ONB2__TriggerSettings__c( Name = 'SubscriptionBeforeInsert' );
        
          // insert Account
           Account account = new Account();
           account.Name = 'TestAccount';
           insert account;
           
        
           // insert subscription
           ONB2__Subscription__c sub = new ONB2__Subscription__c();
           sub.Name = 'testSub';
           sub.ONB2__Status__c = 'Active';
           sub.ONB2__Account__c = account.Id;
           insert sub;
        
           // insert list of Contacts
           List<Contact> contLst = new List<Contact>();
          
           Contact cont1 = new Contact();
           cont1.AccountId = account.Id;
           cont1.LastName = 'TestCon1';
           cont1.FirstName = 'TestContact1'; 
           cont1.Email = 'test1@gmail.com';
           contLst.add(cont1);
          
           Contact cont2 = new Contact();
           cont2.AccountId = account.Id;
           cont2.LastName = 'TestCon2';
           cont2.FirstName = 'TestContact2'; 
           cont2.Email = 'test2@gmail.com';
           contLst.add(cont2);
          
           insert contLst;

      
          List<Contact> contactTest = [SELECT Id,Email FROM Contact WHERE AccountId =: account.Id LIMIT 2];
        
          PageReference testPage = Page.ViewSubPdf;
          testPage.getParameters().put('id', sub.id);
          Test.setCurrentPage(testPage); 
       
          DetailSubsItem controller = new DetailSubsItem(new ApexPages.Standardcontroller(sub)); 
          System.assertEquals(controller.acc, account);
          System.assertEquals(controller.cont, contactTest);
        
          Test.startTest();
       
              controller.sendEmail();
          
              System.assertEquals(controller.addresses,'test1@gmail.com:test2@gmail.com');
              System.assertEquals(controller.toAddresses, new List<String>{'test1@gmail.com','test2@gmail.com'});
 
          Test.stopTest();
    }
          
}